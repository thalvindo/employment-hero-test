import React, {useState, useCallback} from 'react';
// you should import `lodash` as a whole module
import {debounce, isEmpty} from 'lodash';
import axios from 'axios';

const ITEMS_API_URL = 'https://example.com/api/items';
const DEBOUNCE_DELAY = 500;

// the exported component can be either a function or a class

export default function Autocomplete() {
  const [query, setQuery] = useState('');
  const [filteredData, setFilteredData] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const getData = async () => {
    const { data } = await axios.get(ITEMS_API_URL, { params: { q: query } });
    await setIsLoading(false);
    await setFilteredData(data);
  };

  const handleOnChangeText = (e) => {
    
    setIsLoading(true);
    setQuery(e.target.value);

    return debounceHandler();
  };

  const debounceHandler = useCallback(
    debounce(() => {
      getData();
    }, DEBOUNCE_DELAY), []
  );

  return (
    <div className="wrapper">
      <div className={isLoading ? "control" : "is-loading"}>
        <input type="text" className="input" onChange={(e) => {handleOnChangeText(e)}}/>
      </div>
      <div className="list is-hoverable">
        {!isEmpty(filteredData) &&
            filteredData.map((item) => (
              <a class="list-item">{item}</a>
            ))
        }
      </div>
    </div>
  );
}